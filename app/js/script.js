$(document).ready(function () {
    $('.main-carousel').flickity({
        cellAlign: 'center',
        wrapAround: true,
        selectedAttraction: 0.01,
        friction: 0.15,
        resize: true,
        groupCells: 1,
        autoPlay: 4000,
        pageDots: false
    });

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            let forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            let validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();


})
